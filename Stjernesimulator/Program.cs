﻿using System;
using System.Diagnostics;
using System.IO;

namespace Stjernesimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(120, 30);
            Console.WriteLine(@"     _______.___________.    ___      .______         .___________..______          ___       ______  __  ___ ");
            Console.WriteLine(@"    /       |           |   /   \     |   _  \        |           ||   _  \        /   \     /      ||  |/  / ");
            Console.WriteLine(@"   |   (----`---|  |----`  /  ^  \    |  |_)  |       `---|  |----`|  |_)  |      /  ^  \   |  ,----'|  '  /  ");
            Console.WriteLine(@"    \   \       |  |      /  /_\  \   |      /            |  |     |      /      /  /_\  \  |  |     |    <   ");
            Console.WriteLine(@".----)   |      |  |     /  _____  \  |  |\  \----.       |  |     |  |\  \----./  _____  \ |  `----.|  .  \  ");
            Console.WriteLine(@"|_______/       |__|    /__/     \__\ | _| `._____|       |__|     | _| `._____/__/     \__\ \______||__|\__\ ");
            Console.WriteLine(@"                                                                                                              ");
            Console.WriteLine(@"______________________________________________________________________________________________________________");
            Console.WriteLine();
            Console.WriteLine("Type the name of the participant and press <enter>. The timer will start when you press <enter>.");
            var sw = new Stopwatch();
            var doctor = Console.ReadLine();
            var errors = 0;
            Console.WriteLine("The test is now running. Press 'Q' when the participant is done.");
            var endTest = false;
            sw.Start();
            while (true)
            {
                if (endTest)
                {
                    break;
                }

                var input = Console.ReadKey(true);
                switch (input.Key)
                {
                    case ConsoleKey.Q:
                        endTest = true;
                        sw.Stop();
                        break;
                    case ConsoleKey.Spacebar:
                        errors++;
                        Console.WriteLine("Error - in total {0}", errors);
                        break;
                }
            }

            var fileNameBase = String.Format("{0}-{1:dd-MM-yy}", doctor, DateTime.Now);
            var filename = String.Format("{0}.txt", fileNameBase);
            var filepath = AppDomain.CurrentDomain.BaseDirectory + @"\" + filename;
            var fileNameCount = 1;
            while (File.Exists(filepath))
            {
                fileNameBase = String.Format("{0}-{1:dd-MM-yy}", doctor, DateTime.Now);
                filename = String.Format("{0}{1}.txt", fileNameCount, fileNameBase);
                filepath = AppDomain.CurrentDomain.BaseDirectory + @"\" + filename;
                fileNameCount ++;
            }

            using (var outfile = new StreamWriter(filepath))
            {
                outfile.WriteLine("Date: {0:dd-MM-yy}", DateTime.Now);
                outfile.WriteLine("Participant: {0}, errors: {1}, time: {2:g} (in seconds)", doctor, errors, sw.Elapsed.TotalSeconds);
            }

            Console.WriteLine("The test is done. {0} errors in total. Press <enter> to close the program.", errors);
            Console.WriteLine("The result is saved in the file '{0}'", filename);
            Console.ReadLine();
        }
    }
}
